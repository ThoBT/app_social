import { Component, OnInit, Input } from '@angular/core';
import { Comment, ArticleService } from '../../article.service';
import { User } from 'src/app/auth/user.model';

@Component({
  selector: 'app-list-comment',
  templateUrl: './list-comment.component.html',
  styleUrls: ['./list-comment.component.css']
})
export class ListCommentComponent implements OnInit {
  @Input() listComment: Comment[];
  @Input() userCurrent: User;
  @Input() slug: string;
  constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
  }

  removeComment(id: number) {
    // tslint:disable-next-line: triple-equals
    this.listComment = this.listComment.filter(comment => comment.id != id);
    this.articleService.removeComment(this.slug, id);
  }

}
