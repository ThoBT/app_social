export class User {
  constructor(
    public id: string,
    public email: string,
    public username: string,
    public bio: string,
    public image: string,
    // tslint:disable-next-line: variable-name
    private _token: string
  ) {}

  get token() {
    return this._token;
  }
}
