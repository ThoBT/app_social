import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  isLoading = false;
  error: string = null;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    const userName = form.value.username;
    const email = form.value.email.toLowerCase();
    const password = form.value.password;
    this.isLoading = true;
    this.authService.signUp(userName, email, password).subscribe(
      resData => {
        this.isLoading = false;
        this.router.navigate(['/home']);
      },
      errMessage => {
        this.isLoading = false;
        this.error = errMessage;
      }
    );
  }

}
