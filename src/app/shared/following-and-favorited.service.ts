import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Article } from './articles.model';
import { RootProfile } from '../profile/profile.service';

export interface ArticleSingle {
  article: Article;
}

@Injectable({
  providedIn: 'root'
})
export class FollowingAndFavoritedService {
  followingUser = new Subject<boolean>();
  favoritedUser = new Subject<any>();
  private url = 'https://conduit.productionready.io/api';
  constructor(private httpClient: HttpClient) { }

  postFavoritedArticle(slug: string) {
    return this.httpClient.post<ArticleSingle>(`${this.url}/articles/${slug}/favorite`, {});
  }

  removeFavoritedArticle(slug: string) {
    return this.httpClient.delete<ArticleSingle>(`${this.url}/articles/${slug}/favorite`);
  }

  followingUserByUsername(username: string) {
    return this.httpClient.post<RootProfile>(`${this.url}/profiles/${username}/follow`, {});
  }

  UnfollowingUserByUsername(username: string) {
    return this.httpClient.delete<RootProfile>(`${this.url}/profiles/${username}/follow`);
  }
}
