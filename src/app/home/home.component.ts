import { Component, OnInit, OnDestroy } from '@angular/core';
import { HomeService } from './home.service';
import { Article, Articles } from '../shared/articles.model';
import { AuthService } from '../auth/auth.service';
import { Subscription, Observable, forkJoin } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  nameFeedMode = 'globalFeed';
  isLoadingMode = false;
  isUser = false;
  listTag = [];
  listArticle: Article[] = [];
  articleCount: number;
  nameTag: string;
  sub: Subscription;
  constructor(
    private homeService: HomeService,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    forkJoin(
      this.homeService.fetchArticles(),
      this.homeService.fecthTags()
    ).subscribe(([res1, res2]) => {
      this.articleCount = res1.articlesCount;
      this.listArticle = res1.articles;
      this.listTag = res2.tags;
    });

    // this.homeService.fetchArticles().subscribe(resData => {
    //   this.listArticle = resData.articles;
    //   this.articleCount = resData.articlesCount;
    // });

    // this.homeService.fecthTags().subscribe(resData => {
    //   this.listTag = resData.tags;
    // });

    this.sub = this.authService.user.subscribe(user => {
      this.isUser = user ? true : false;
    });
  }

  switchMode(name: string) {
    if (name === 'yourFeed') {
      this.nameFeedMode = 'yourFeed';
      this.handleArticle(this.nameFeedMode);
    } else {
      this.nameFeedMode = 'globalFeed';
      this.handleArticle(this.nameFeedMode);
    }
  }

  onShowTag(item: string) {
    this.nameFeedMode = 'tagsFeed';
    this.nameTag = item;
    this.handleArticle(this.nameFeedMode, item);
  }

  getPage(offset: number) {
    let resOfHome: Observable<Articles>;
    switch (this.nameFeedMode) {
      case 'yourFeed':
        resOfHome = this.homeService.fecthYourFeed(String(offset));
        break;
      case 'globalFeed':
        resOfHome = this.homeService.fetchArticles(String(offset));
        break;
      case 'tagsFeed':
        resOfHome = this.homeService.fetchTagsFeed(this.nameTag, String(offset));
        break;
    }

    resOfHome.subscribe(resData => {
      this.listArticle = resData.articles;
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    });
  }

  private configArticle() {
    this.listArticle = [];
    this.isLoadingMode = true;
  }

  private handleArticle(name?: string, item?: string) {
    let resOfHome: Observable<Articles>;
    switch (name) {
      case 'yourFeed':
        this.configArticle();
        resOfHome = this.homeService.fecthYourFeed();
        break;
      case 'globalFeed':
        this.configArticle();
        resOfHome = this.homeService.fetchArticles();
        break;
      case 'tagsFeed':
        this.configArticle();
        resOfHome = this.homeService.fetchTagsFeed(item);
        break;
    }

    resOfHome.subscribe(resData => {
      this.isLoadingMode = false;
      this.listArticle = resData.articles;
      this.articleCount = resData.articlesCount;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
