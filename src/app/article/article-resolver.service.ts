import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ArticleService, SingleArticle } from './article.service';

@Injectable({providedIn: 'root'})
export class ArticleResolverService implements Resolve<SingleArticle> {
  constructor(private articleService: ArticleService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SingleArticle> | Promise<SingleArticle> | SingleArticle {
    return this.articleService.fetchArticleBySlug(route.params.slug);
  }
}
