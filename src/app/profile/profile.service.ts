import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Articles } from '../shared/articles.model';

export interface Profile {
  username: string;
  bio: string;
  image: string;
  following: boolean;
}

export interface RootProfile {
  profile: Profile;
}
@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private url = 'https://conduit.productionready.io/api';
  constructor(private httpClient: HttpClient) {}

  fetchProfileByUsername(username: string) {
    return this.httpClient.get<RootProfile>(`${this.url}/profiles/${username}`);
  }

  fetchMyArticle(username: string, offset = '0') {
    return this.httpClient.get<Articles>(`${this.url}/articles`, {
      params: new HttpParams().set('author', username).set('limit', '10').set('offset', offset)
    });
  }

  fetchMyArticleByFavorites(username: string, offset = '0') {
    return this.httpClient.get<Articles>(`${this.url}/articles`, {
      params: new HttpParams().set('favorited', username).set('limit', '10').set('offset', offset)
    });
  }
}
