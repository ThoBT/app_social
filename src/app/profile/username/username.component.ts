import { Component, OnInit, Input, OnDestroy, DoCheck } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Profile } from '../profile.service';
import { AuthService } from 'src/app/auth/auth.service';
import { FollowingAndFavoritedService } from '../../shared/following-and-favorited.service';

@Component({
  selector: 'app-username',
  templateUrl: './username.component.html',
  styleUrls: ['./username.component.css']
})
export class UsernameComponent implements OnInit, OnDestroy, DoCheck {
  @Input() profile: Profile;
  @Input() username: string;
  isUser = true;
  subscription: Subscription[] = [];
  constructor(
    private router: Router,
    private authService: AuthService,
    private followAndFavoriteService: FollowingAndFavoritedService
  ) {}

  ngOnInit(): void {
    this.subscription.push(this.followAndFavoriteService.followingUser.subscribe(data => {
      this.profile.following = data;
    }));
  }

  ngDoCheck() {
    this.subscription.push( this.authService.user.subscribe(resData => {
      this.isUser = resData?.username === this.username ? true : false;
    }));
  }

  navigateSetting() {
    this.router.navigate(['/setting']);
  }

  ngOnDestroy() {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
