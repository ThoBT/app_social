import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { SideBarHomeComponent } from './side-bar-home/side-bar-home.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [HomeComponent, SideBarHomeComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([{ path: '', component: HomeComponent }])
  ]
})
export class HomeModule {}
