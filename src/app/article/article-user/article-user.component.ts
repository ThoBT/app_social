import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Article } from 'src/app/shared/articles.model';
import { FollowingAndFavoritedService } from '../../shared/following-and-favorited.service';
import { AuthService } from 'src/app/auth/auth.service';
import { ArticleService } from '../article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-user',
  templateUrl: './article-user.component.html',
  styleUrls: ['./article-user.component.css']
})
export class ArticleUserComponent implements OnInit, OnDestroy {
  @Input() article: Article;
  isUser: boolean;
  subscription: Subscription[] = [];
  constructor(
    private router: Router,
    private followAndFavoriteService: FollowingAndFavoritedService,
    private authService: AuthService,
    private articleService: ArticleService
  ) {}

  ngOnInit(): void {
    this.subscription.push(
      this.followAndFavoriteService.followingUser.subscribe(data => {
        this.article.author.following = data;
      })
    );

    this.subscription.push(
      this.followAndFavoriteService.favoritedUser.subscribe(data => {
        this.article.favorited = data.resData.article.favorited;
        this.article.favoritesCount = data.resData.article.favoritesCount;
      })
    );

    this.authService.user.subscribe(user => {
      this.isUser =
        user?.username === this.article.author.username ? true : false;
    });
  }

  removeArticle() {
    this.articleService.removeArticle(this.article.slug).subscribe(data => {
      this.router.navigate(['/home']);
    });
  }

  ngOnDestroy() {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
