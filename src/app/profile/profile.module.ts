import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ProfileComponent } from './profile.component';
import { UsernameComponent } from './username/username.component';
import { RouterModule } from '@angular/router';
import { ProfileResolverService } from './profile-resolver.service';

@NgModule({
  declarations: [ProfileComponent, UsernameComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProfileComponent,
        resolve: { user: ProfileResolverService }
      }
    ])
  ]
})
export class ProfileModule {}
