import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/user.model';
import { ArticleService, Comment } from '../article.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit, OnDestroy {
  @Input() slug: string;
  user: User;
  listComment: Comment[] = [];
  subscription: Subscription;
  constructor(private authService: AuthService, private articleService: ArticleService) { }

  ngOnInit(): void {
    this.subscription = this.authService.user.subscribe(user => {
      this.user = user;
    });

    this.articleService.fetchCommentsToArticle(this.slug).subscribe(resData => {
      this.listComment = resData.comments;
    });
  }

  addComment(comment) {
    if (!comment.value) {
      return;
    }
    this.articleService.addCommentToArticle(comment.value, this.slug).subscribe(resData => {
      this.listComment.unshift(resData.comment);
    });
    comment.value = '';
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
