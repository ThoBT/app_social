import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-spiner',
  template: `
    <div class="lds-hourglass"></div>
  `,
  styleUrls: ['./loading-spiner.component.css']
})
export class LoadingSpinerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
