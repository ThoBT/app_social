import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ProfileService, RootProfile } from './profile.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ProfileResolverService implements Resolve<RootProfile> {
  constructor(private profileService: ProfileService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RootProfile> | Promise<RootProfile> | RootProfile {
    return this.profileService.fetchProfileByUsername(route.params.username);
  }
}
