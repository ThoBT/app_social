import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';

interface ItemData {
  id: string;
  email: string;
  username: string;
  bio: string;
  image: string;
  token: string;
}

export interface AuthResponseData {
  user: ItemData;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user        = new BehaviorSubject<User>(null);
  private url = 'https://conduit.productionready.io/api';
  constructor(private httpClient: HttpClient, private router: Router) {}

  login(email: string, password: string) {
    const user = {
      user: {
        email,
        password
      }
    };
    return this.httpClient
      .post<AuthResponseData>(`${this.url}/users/login`, user)
      .pipe(
        catchError(this.handleError),
        tap(resData => {
          this.handleAuthentication(
            resData.user.id,
            resData.user.email,
            resData.user.username,
            resData.user.bio,
            resData.user.image,
            resData.user.token
          );
        })
      );
  }

  signUp(username: string, email: string, password: string) {
    const user = {
      user: {
        username,
        email,
        password
      }
    };

    return this.httpClient
      .post<AuthResponseData>(`${this.url}/users`, user)
      .pipe(
        catchError(this.handleError),
        tap(resData => {
          this.handleAuthentication(
            resData.user.id,
            resData.user.email,
            resData.user.username,
            resData.user.bio,
            resData.user.image,
            resData.user.token
          );
        })
      );
  }

  updateUser(body: any) {
    return this.httpClient
      .put<AuthResponseData>(`${this.url}/user`, body)
      .pipe(
        tap(resData => {
          this.handleAuthentication(
            resData.user.id,
            resData.user.email,
            resData.user.username,
            resData.user.bio,
            resData.user.image,
            resData.user.token
          );
        })
      );
  }

  logout() {
    this.user.next(null);
    localStorage.removeItem('userData');
    this.router.navigate(['/login']);
  }

  autoLogin() {
    const userData: {
      id: string;
      email: string;
      username: string;
      bio: string;
      image: string;
      _token: string;
    } = JSON.parse(localStorage.getItem('userData'));

    if (!userData) {
      return;
    }

    const loaderUser = new User(
      userData.id,
      userData.email,
      userData.username,
      userData.bio,
      userData.image,
      userData._token
    );
    if (loaderUser.token) {
      this.user.next(loaderUser);
    }
  }

  private handleAuthentication(
    id: string,
    email: string,
    username: string,
    bio: string,
    image: string,
    token: string
  ) {
    const user = new User(id, email, username, bio, image, token);
    this.user.next(user);
    localStorage.setItem('userData', JSON.stringify(user));
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = 'An unknown error occurred';
    // tslint:disable-next-line: forin
    for (const err in errorRes.error.errors) {
      switch (errorRes.error.errors[err][0]) {
        case 'has already been taken':
          errorMessage = 'Email or Username has already been taken';
          break;
        case 'is too short (minimum is 8 characters)':
          errorMessage = 'Password is too short (minimum is 8 characters)';
          break;
        case 'is invalid':
          errorMessage = 'Email or password is invalid';
          break;
      }
    }
    return throwError(errorMessage);
  }
}
