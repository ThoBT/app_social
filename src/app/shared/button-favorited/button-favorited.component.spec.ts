import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonFavoritedComponent } from './button-favorited.component';

describe('ButtonFavoritedComponent', () => {
  let component: ButtonFavoritedComponent;
  let fixture: ComponentFixture<ButtonFavoritedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonFavoritedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonFavoritedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
