import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LoadingSpinerComponent } from './loading-spiner/loading-spiner.component';
import { ListArticleComponent } from './list-article/list-article.component';
import { ButtonFollowingComponent } from './button-following/button-following.component';
import { ButtonFavoritedComponent } from './button-favorited/button-favorited.component';
import { PaginationComponent } from './pagination/pagination.component';


@NgModule({
  declarations: [
    LoadingSpinerComponent,
    ListArticleComponent,
    ButtonFollowingComponent,
    ButtonFavoritedComponent,
    PaginationComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    LoadingSpinerComponent,
    ListArticleComponent,
    ButtonFollowingComponent,
    ButtonFavoritedComponent,
    PaginationComponent,
    CommonModule
  ]
})

export class SharedModule {}
