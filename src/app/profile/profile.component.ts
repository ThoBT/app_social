import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProfileService, Profile } from './profile.service';
import { Article, Articles } from '../shared/articles.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  nameProfileMode = 'myFavorites';
  isLoadingMode = false;
  isFollowingMode = false;
  username: string;
  listArticle: Article[] = [];
  profile: Profile;
  articleCount: number;
  constructor(
    private route: ActivatedRoute,
    private profileService: ProfileService
  ) {}

  ngOnInit(): void {
    this.route.data
      .pipe(
        mergeMap(data => {
          this.profile = data.user.profile;
          this.username = data.user.profile.username;
          return this.profileService.fetchMyArticle(this.username);
        })
      )
      .subscribe(resData => {
        this.listArticle = resData.articles;
        this.articleCount = resData.articlesCount;
      });
  }

  getPage(offset: number) {
    let resOfHome: Observable<Articles>;
    switch (this.nameProfileMode) {
      case 'favoritesArticle':
        resOfHome = this.profileService.fetchMyArticleByFavorites(this.username, String(offset));
        break;
      case 'myFavorites':
        resOfHome = this.profileService.fetchMyArticle(this.username, String(offset));
        break;
    }

    resOfHome.subscribe(resData => {
      this.listArticle = resData.articles;
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    });
  }

  switchArticleMode(name: string) {
    let resOfProfile: Observable<Articles>;

    if (name === 'favoritesArticle') {
      this.configProfile(name);
      resOfProfile = this.profileService.fetchMyArticleByFavorites(
        this.username
      );
    } else {
      this.configProfile(name);
      resOfProfile = this.profileService.fetchMyArticle(this.username);
    }

    resOfProfile.subscribe(resData => {
      this.isLoadingMode = false;
      this.listArticle = resData.articles;
      this.articleCount = resData.articlesCount;
    });
  }

  private configProfile(name) {
    this.nameProfileMode = name;
    this.listArticle = [];
    this.isLoadingMode = true;
  }
}
