import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { FollowingAndFavoritedService, ArticleSingle } from '../following-and-favorited.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-button-favorited',
  templateUrl: './button-favorited.component.html',
  styleUrls: ['./button-favorited.component.css']
})
export class ButtonFavoritedComponent implements OnInit, OnDestroy {
  @Input() slug: string;
  @Input() index?: number;
  @Input() isFavorited: boolean;
  isUser: boolean;
  sub: Subscription;
  constructor(
    private router: Router,
    private authService: AuthService,
    private followAndFavoriteService: FollowingAndFavoritedService
  ) {}

  ngOnInit(): void {
    this.sub = this.authService.user.subscribe(user => {
      this.isUser = user ? true : false;
    });
  }

  onFavoritedArticle() {
    let ob: Observable<ArticleSingle>;
    if (this.isUser) {
      if (this.isFavorited === false) {
        ob = this.followAndFavoriteService.postFavoritedArticle(this.slug);
      } else {
        ob = this.followAndFavoriteService.removeFavoritedArticle(this.slug);
      }

      ob.subscribe(resData => {
        this.followAndFavoriteService.favoritedUser.next({
          resData,
          index: this.index
        });
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
