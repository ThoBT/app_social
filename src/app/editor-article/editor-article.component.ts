import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ArticleService } from '../article/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-editor-article',
  templateUrl: './editor-article.component.html',
  styleUrls: ['./editor-article.component.css']
})
export class EditorArticleComponent implements OnInit {
  editorForm: FormGroup;
  tagList = [];
  hasSlug: boolean;
  slug: string;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private articleService: ArticleService
  ) {}

  ngOnInit(): void {
    this.route.firstChild?.params.subscribe((params) => {
      this.hasSlug = params.slug ? true : false;
      this.slug   = params.slug;
    });
    this.initForm();
  }

  onSubmit() {
    const article = {
      article: {
        ...this.editorForm.value,
        tagList: this.tagList
      }
    };

    if (this.hasSlug) {
      this.articleService.updateArticle(article, this.slug).subscribe(res => {
        this.router.navigate(['/article', res.article.slug]);
      });
    } else {
      this.articleService.createArticle(article).subscribe(data => {
        this.router.navigate(['/article', data.article.slug]);
      });
    }

  }

  addTag(tag) {
    this.tagList.push(tag.value);
    tag.value = '';
  }

  removeTag(index: number) {
    this.tagList.splice(index, 1);
  }

  private initForm() {
    const title       = '';
    const description = '';
    const body        = '';

    if (this.hasSlug) {
      this.articleService.fetchArticleBySlug(this.slug).subscribe(res => {
        this.editorForm = this.fb.group({
          title: [ res.article.title, Validators.required],
          description: [res.article.description, Validators.required],
          body: [res.article.body, Validators.required]
        });
        this.tagList = res.article.tagList;
      });
    }
    this.editorForm = this.fb.group({
      title      : [title, Validators.required],
      description: [description, Validators.required],
      body       : [body, Validators.required]
    });
  }
}
