import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  settingForm: FormGroup;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  onlogOut() {
    this.authService.logout();
  }

  onSubmit() {
    const email    = this.settingForm.value.email;
    const username = this.settingForm.value.username;
    const bio      = this.settingForm.value.bio;
    const image    = this.settingForm.value.urlImage;
    const user = {
      user: {
        username,
        email,
        bio,
        image
      }
    };
    this.authService.updateUser(user).subscribe(res => {
      this.router.navigate(['/profile', res.user.username]);
    });
  }

  private initForm() {
    let urlImage   = '';
    let username   = '';
    let bio        = '';
    let email      = '';
    const password = '';

    this.authService.user.subscribe(user => {
      urlImage = user?.image;
      username = user?.username;
      bio      = user?.bio;
      email    = user?.email;
    });

    this.settingForm = this.fb.group({
      urlImage: [urlImage, Validators.required],
      username: [username, Validators.required],
      bio     : [bio, Validators.required],
      email   : [email, Validators.email],
      password: [password]
    });
  }
}
