import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { ArticleComponent } from './article.component';
import { ArticleUserComponent } from './article-user/article-user.component';
import { ArticleContentComponent } from './article-content/article-content.component';
import { CommentComponent } from './comment/comment.component';
import { ListCommentComponent } from './comment/list-comment/list-comment.component';
import { ArticleResolverService } from './article-resolver.service';

@NgModule({
  declarations: [
    ArticleComponent,
    ArticleUserComponent,
    ArticleContentComponent,
    CommentComponent,
    ListCommentComponent,
  ],
  imports: [
    RouterModule.forChild([
      {path: '', component: ArticleComponent, resolve: {article: ArticleResolverService}},
    ]),
    SharedModule
  ]
})

export class ArticleModule {}
