import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FollowingAndFavoritedService } from '../following-and-favorited.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';
import { RootProfile } from 'src/app/profile/profile.service';

@Component({
  selector: 'app-button-following',
  templateUrl: './button-following.component.html',
  styleUrls: ['./button-following.component.css']
})
export class ButtonFollowingComponent implements OnInit, OnDestroy {
  @Input() userName: string;
  @Input() isFollow: boolean;
  isUser: boolean;
  sub: Subscription;
  constructor(
    private router: Router,
    private authService: AuthService,
    private followAndFavoriteService: FollowingAndFavoritedService
  ) {}

  ngOnInit(): void {
    this.sub = this.authService.user.subscribe(user => {
      this.isUser = user ? true : false;
    });
  }

  onFollowUser() {
    if (this.isUser) {
      let ob: Observable<RootProfile>;
      if (this.isFollow === false) {
        ob = this.followAndFavoriteService.followingUserByUsername(
          this.userName
        );
      } else {
        ob = this.followAndFavoriteService.UnfollowingUserByUsername(
          this.userName
        );
      }

      ob.subscribe(resData => {
        this.followAndFavoriteService.followingUser.next(
          resData.profile.following
        );
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
