import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Data } from "@angular/router";
import { Article } from "../shared/articles.model";
import { AuthService } from "../auth/auth.service";

@Component({
  selector: "app-article",
  templateUrl: "./article.component.html",
  styleUrls: ["./article.component.css"]
})
export class ArticleComponent implements OnInit {
  article: Article;
  hasUser: boolean;
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.authService.user.subscribe(user => {
      this.hasUser = user ? false : true;
    });

    this.route.data.subscribe((data: Data) => {
      this.article = data.article.article;
    });
  }
}
