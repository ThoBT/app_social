import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonFollowingComponent } from './button-following.component';

describe('ButtonFollowingComponent', () => {
  let component: ButtonFollowingComponent;
  let fixture: ComponentFixture<ButtonFollowingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonFollowingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonFollowingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
