import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PaginateService } from '../paginate.service';
import { Article } from '../articles.model';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Input() listArticle: Article[] = [];
  @Input() articleCount: number;
  @Output() nextOffset = new EventEmitter<number>();
  currentPage = 1;
  pager: any = {};
  constructor(private paginateService: PaginateService) { }

  ngOnInit() {
    console.log(this.articleCount);

    this.pager = this.paginateService.getPager(this.articleCount, this.currentPage);
  }

  setPage(page: number) {
    this.pager = this.paginateService.getPager(this.articleCount, page);
    this.nextOffset.emit(this.pager.offset);
  }


}
