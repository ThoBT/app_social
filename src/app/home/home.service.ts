import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Articles } from '../shared/articles.model';

interface Tags {
  tags: [];
}

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private url = 'https://conduit.productionready.io/api';
  constructor(private httpClient: HttpClient) { }

  fetchArticles(offset = '0') {
    return this.httpClient.get<Articles>(`${this.url}/articles`, {
      params: new HttpParams().set('limit', '10').set('offset', offset)
    });
  }

  fecthYourFeed(offset = '0') {
    return this.httpClient.get<Articles>(`${this.url}/articles/feed`, {
      params: new HttpParams().set('limit', '10').set('offset', offset)
    });
  }

  fetchTagsFeed(tag: string, offset = '0') {
    return this.httpClient.get<Articles>(`${this.url}/articles`, {
      params: new HttpParams().set('tag', tag).set('limit', '10').set('offset', offset)
    });
  }

  fecthTags() {
    return this.httpClient.get<Tags>(`${this.url}/tags`);
  }

}
