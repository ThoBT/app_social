import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Article } from '../articles.model';
import { FollowingAndFavoritedService } from '../following-and-favorited.service';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit, OnDestroy {
  @Input() listArticle: Article[];
  @Input() isLoadingMode: boolean;
  favoriteSub: Subscription;
  constructor(private followAndFavoriteService: FollowingAndFavoritedService) { }

  ngOnInit(): void {
    this.favoriteSub = this.followAndFavoriteService.favoritedUser.subscribe(data => {
      this.listArticle[data.index] = data.resData.article;
    });
  }

  ngOnDestroy() {
    this.favoriteSub.unsubscribe();
  }

}
