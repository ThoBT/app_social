import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article, Author } from '../shared/articles.model';


export interface Comment {
  id: number;
  createdAt: string;
  updatedAt: string;
  body: string;
  author: Author;
}

interface CommentOfPost {
  comment: Comment;
}

export interface RootComments {
  comments: Comment[];
}
export interface SingleArticle {
  article: Article;
}

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private url = 'https://conduit.productionready.io/api';
  constructor(private httpClient: HttpClient) {}


  createArticle(body) {
    return this.httpClient.post<SingleArticle>(`${this.url}/articles`, body);
  }

  updateArticle(body, slug: string) {
    return this.httpClient.put<SingleArticle>(`${this.url}/articles/${slug}`, body);
  }

  removeArticle(slug: string) {
    return this.httpClient.delete(`${this.url}/articles/${slug}`);
  }

  fetchArticleBySlug(slug: string) {
    return this.httpClient.get<SingleArticle>(`${this.url}/articles/${slug}`);
  }

  addCommentToArticle(content: string, slug: string) {
    const body = {
      comment: {
        body: content
      }
    };
    return this.httpClient.post<CommentOfPost>(`${this.url}/articles/${slug}/comments`, body);
  }

  fetchCommentsToArticle(slug: string) {
    return this.httpClient.get<RootComments>(`${this.url}/articles/${slug}/comments`);
  }

  removeComment(slug: string, id: number) {
    return this.httpClient.delete(`${this.url}/articles/${slug}/comments/${id}`).subscribe();
  }
}
