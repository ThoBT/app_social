import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticate = false;
  user: User;
  authSub: Subscription;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authSub = this.authService.user.subscribe(user => {
      this.isAuthenticate = user ? true : false;
      this.user = user;
    });
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }

}
