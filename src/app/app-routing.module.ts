import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { SettingComponent } from './setting/setting.component';
import { EditorArticleComponent } from './editor-article/editor-article.component';
import { AuthGuard } from './auth/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)},
  {path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  {path: 'editor', component: EditorArticleComponent, canActivate: [AuthGuard], children: [
    {path: ':slug', component: EditorArticleComponent}
  ]},
  {path: 'setting', component: SettingComponent, canActivate: [AuthGuard]},
  {path: 'profile/:username', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)},
  {path: 'article/:slug', loadChildren: () => import('./article/article.module').then(m => m.ArticleModule)},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
